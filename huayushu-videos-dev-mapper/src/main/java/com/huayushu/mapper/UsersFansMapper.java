package com.huayushu.mapper;

import com.huayushu.pojo.UsersFans;
import com.huayushu.utils.MyMapper;

public interface UsersFansMapper extends MyMapper<UsersFans> {
}