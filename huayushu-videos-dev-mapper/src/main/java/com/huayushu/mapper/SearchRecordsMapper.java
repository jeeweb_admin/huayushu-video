package com.huayushu.mapper;

import com.huayushu.pojo.SearchRecords;
import com.huayushu.utils.MyMapper;

import java.util.List;

public interface SearchRecordsMapper extends MyMapper<SearchRecords> {
    public List<String> getHotwords();
}