package com.huayushu.mapper;

import com.huayushu.pojo.Videos;
import com.huayushu.utils.MyMapper;

public interface VideosMapper extends MyMapper<Videos> {
}