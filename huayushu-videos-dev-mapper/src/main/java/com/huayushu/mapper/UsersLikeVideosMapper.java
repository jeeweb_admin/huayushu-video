package com.huayushu.mapper;

import com.huayushu.pojo.UsersLikeVideos;
import com.huayushu.utils.MyMapper;

public interface UsersLikeVideosMapper extends MyMapper<UsersLikeVideos> {
}