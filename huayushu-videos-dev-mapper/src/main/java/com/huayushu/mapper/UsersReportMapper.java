package com.huayushu.mapper;

import com.huayushu.pojo.UsersReport;
import com.huayushu.utils.MyMapper;

public interface UsersReportMapper extends MyMapper<UsersReport> {
}