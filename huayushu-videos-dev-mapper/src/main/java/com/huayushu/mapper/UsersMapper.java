package com.huayushu.mapper;

import com.huayushu.pojo.Users;
import com.huayushu.utils.MyMapper;

public interface UsersMapper extends MyMapper<Users> {
    /**
     * 用户受喜欢数增加
     * @param userId
     */
    public void addReceiveLikeCount(String userId);

    /**
     * 用户受喜欢数累减
     * @param userId
     */
    public void reduceReceiveLikeCount(String userId);

    /**
     * 增加粉丝数量
     * @param userId
     */
    public void addFansCount(String userId);

    /**
     * 减少粉丝数量
     * @param userId
     */
    public void reduceFansCount(String userId);

    /**
     * 增加关注数
     * @param userId
     */
    public void addFollersCount(String userId);

    /**
     * 减少关注数
     * @param userId
     */
    public void reduceFollersCount(String userId);

}