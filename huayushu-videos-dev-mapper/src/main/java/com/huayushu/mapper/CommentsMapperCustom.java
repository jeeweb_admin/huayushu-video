package com.huayushu.mapper;

import com.huayushu.pojo.Comments;
import com.huayushu.pojo.vo.CommentsVO;
import com.huayushu.utils.MyMapper;

import java.util.List;

/**
 * @author: huayushu luming
 * @date: 2020-01-17 01:18
 * @desc:
 **/
public interface CommentsMapperCustom extends MyMapper<Comments> {
    public List<CommentsVO>queryComments(String videoId);
}
