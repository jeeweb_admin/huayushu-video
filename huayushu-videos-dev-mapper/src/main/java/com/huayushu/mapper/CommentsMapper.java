package com.huayushu.mapper;

import com.huayushu.pojo.Comments;
import com.huayushu.utils.MyMapper;

public interface CommentsMapper extends MyMapper<Comments> {
}