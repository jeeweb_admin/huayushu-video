package com.huayushu.mapper;

import com.huayushu.pojo.Videos;
import com.huayushu.pojo.vo.VideosVO;
import com.huayushu.utils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VideosMapperCustom extends MyMapper<Videos> {
    /**
     * 查询所有视频列表
     * @param videoDesc
     * @param userId
     * @return
     */
    public List<VideosVO>queryAllVideos(@Param("videoDesc") String videoDesc,@Param("userId")String userId);

    /**
     * 对喜欢的视频进行点赞
     * @param videoId
     */
    public void addVideoLikeCount(String videoId);

    /**
     * 对已经点赞过得视频取消点赞
     * @param videoId
     */
    public void reduceVideoLikeCount(String videoId);

    /**
     * 查询关注的视频
     * @return
     */
    public List<VideosVO> queryMyFollowVideos(String userId);

    /**
     * 查询点赞视频
     * @return
     */
    public List<VideosVO> queryMyLikeVideos(@Param("userId") String userId);
}