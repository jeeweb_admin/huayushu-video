package com.huayushu.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: huayushu luming
 * @date: 2019-12-02 22:31
 * @desc:  生成视频截图工具类
 **/
public class FetchVideoCover {
    private String ffmpegExE;

    public FetchVideoCover(String ffmpegExE) {
        super();
        this.ffmpegExE = ffmpegExE;
    }

    public void getCover(String videoInputPath, String coverOutputPath) throws IOException, InterruptedException {
//		ffmpeg.exe -ss 00:00:01 -i spring.mp4 -vframes 1 bb.jpg
        List<String> command = new java.util.ArrayList<String>();
        command.add(ffmpegExE);

        // 指定截取第1秒
        command.add("-ss");
        command.add("00:00:01");

        command.add("-y");
        command.add("-i");
        command.add(videoInputPath);

        command.add("-vframes");
        command.add("1");

        command.add(coverOutputPath);

        for (String c : command) {
            System.out.print(c + " ");
        }

        ProcessBuilder builder = new ProcessBuilder(command);
        Process process = builder.start();

        InputStream errorStream = process.getErrorStream();
        InputStreamReader inputStreamReader = new InputStreamReader(errorStream);
        BufferedReader br = new BufferedReader(inputStreamReader);

        String line = "";
        while ( (line = br.readLine()) != null ) {
        }

        if (br != null) {
            br.close();
        }
        if (inputStreamReader != null) {
            inputStreamReader.close();
        }
        if (errorStream != null) {
            errorStream.close();
        }
    }

    public static void main(String[]args){

        FetchVideoCover ffmpeg=new FetchVideoCover("D:\\soft\\ffmpeg\\bin\\ffmpeg.exe");
        try {
            ffmpeg.getCover("E:\\huayushu_video_dev\\1001\\video\\打击.mp4","E:\\huayushu_video_dev\\1001\\zwdar.jpg");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
