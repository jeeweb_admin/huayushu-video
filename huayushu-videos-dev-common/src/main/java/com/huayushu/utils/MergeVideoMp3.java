package com.huayushu.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: huayushu luming
 * @date: 2019-12-02 22:31
 * @desc:  此工具类待改进，有背景音乐的无法合并
 **/
public class MergeVideoMp3 {
    private String ffmpegExE;

    public MergeVideoMp3(String ffmpegExE) {
        super();
        this.ffmpegExE = ffmpegExE;
    }

    public void convertor(String videoInputPath, String mp3InputPath,
                          double seconds, String videoOutputPath) throws Exception {
//		ffmpeg.exe -i 苏州大裤衩.mp4 -i bgm.mp3 -t 7 -y 新的视频.mp4
        List<String> command = new ArrayList<>();
        command.add(ffmpegExE);

        command.add("-i");
        command.add(videoInputPath);

        command.add("-i");
        command.add(mp3InputPath);

        command.add("-t");
        command.add(String.valueOf(seconds));

        command.add("-y");
        command.add(videoOutputPath);

//		for (String c : command) {
//			System.out.print(c + " ");
//		}

        ProcessBuilder builder = new ProcessBuilder(command);
        Process process = builder.start();

        InputStream errorStream = process.getErrorStream();
        InputStreamReader inputStreamReader = new InputStreamReader(errorStream);
        BufferedReader br = new BufferedReader(inputStreamReader);

        String line = "";
        while ( (line = br.readLine()) != null ) {
        }

        if (br != null) {
            br.close();
        }
        if (inputStreamReader != null) {
            inputStreamReader.close();
        }
        if (errorStream != null) {
            errorStream.close();
        }

    }

    public static void main(String[]args){

        MergeVideoMp3 ffmpeg=new MergeVideoMp3("D:\\soft\\ffmpeg\\bin\\ffmpeg.exe");
        try {
            ffmpeg.convertor("E:\\huayushu_video_dev\\1001\\video\\打击.mp4","E:\\huayushu_video_dev\\bgm\\zwdar.mp3",17.1,"E:\\huayushu_video_dev\\xx.mp4");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
