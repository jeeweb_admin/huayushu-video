package com.huayushu.service;

import com.huayushu.pojo.Users;
import com.huayushu.pojo.UsersReport;

public interface UserService {
    /**
     * 判断用户名是否存在
     * @param username
     * @return boolean
     */
    boolean queryUsernameIsExist(String username);

    /**
     * 保存用户信息
     * @param user
     */
     void saveUser(Users user);

    /**
     * 根据用户名密码查询用户
     * @param username
     * @param password
     * @return
     */
     Users queryUserForLogin(String username,String password);

    /**
     * 更新用户信息
     * @param users
     */
     void updateUserInfo(Users users);

    /**
     * 查询用户信息
     * @param userId
     * @return
     */
     Users queryUserInfo(String userId);

    /**
     * 查询用户是否喜欢当前视频
     * @param userId
     * @param videoId
     * @return
     */
     boolean isUserLikeVideo(String userId,String videoId);

    /**
     * 增加用户和粉丝关系
     * @param userId
     * @param fanId
     */
     void saveUserFanRelation(String userId,String fanId);

    /**
     * 删除用户和粉丝关系
     * @param userId
     * @param fanId
     */
     void deleteUserFanRelation(String userId,String fanId);

    /**
     * 查询用户是否关注
     * @param userId
     * @param fanId
     * @return
     */
     boolean queryIfFollw(String userId,String fanId);

    /**
     * 举报用户
     * @param usersReport
     */
     void reportUser(UsersReport usersReport);
}
