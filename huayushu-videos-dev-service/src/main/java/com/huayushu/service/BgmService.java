package com.huayushu.service;

import com.huayushu.pojo.Bgm;

import java.util.List;

public interface BgmService {
    /**
     * 查询背景音乐列表
     * @return List<Bgm>
     */
    List<Bgm> queryBgmList();

    /**
     * 根据id查询bgm
     * @param bgmId
     * @return
     */
    Bgm queryBgmById(String bgmId);
}
