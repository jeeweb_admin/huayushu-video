package com.huayushu.service;

import com.huayushu.pojo.Comments;
import com.huayushu.pojo.Videos;
import com.huayushu.utils.PagedResult;

import java.util.List;

public interface VideoService {
    /**
     * 保存视频
     * @param videos
     * @return
     */
     String  saveVideo(Videos videos);

    /**
     * 更新视频封面
     * @param videoId
     * @param coverPath
     * @return
     */
    void updateVideo(String videoId,String coverPath);

    /**
     * 分页查询视频列表
     * @param page
     * @parm pageSizeP
     * @return
     */
    PagedResult getAllVideos(Videos videos,Integer isSaveRecord,Integer page, Integer pageSize);

    /**
     * 获取热搜词列表
     * @return
     */
    List<String> getHotwords();

    /**
     *  查询我喜欢的视频列表
     */
     PagedResult queryMyLikeVideos(String userId, Integer page, Integer pageSize);

    /**
     * 查询我关注的人的视频列表
     */
     PagedResult queryMyFollowVideos(String userId, Integer page, Integer pageSize);
    /**
     * 用户点赞视频
     * @param userId
     */
    void userLikeVideo(String userId, String videoId, String videoCreaterId);

    /**
     * 用户不喜欢视频
     * @param userId
     * @param videoId
     * @param videoCreaterId
     */
    void userUnLikeVideo(String userId,String videoId,String videoCreaterId);

    /**
     * 保存留言
     * @param comments
     */
    void saveComment(Comments comments);

    /**
     * 查询评论列表
     * @param videoId
     * @param page
     * @param pageSize
     * @return
     */
    PagedResult getAllComments(String videoId,Integer page,Integer pageSize);
}
