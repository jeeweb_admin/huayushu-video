package com.huayushu.service.impl;

import com.huayushu.mapper.BgmMapper;
import com.huayushu.pojo.Bgm;
import com.huayushu.service.BgmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: huayushu luming
 * @date: 2019-11-29 01:29
 * @desc:
 **/
@Service
public class BgmServiceImpl implements BgmService {

    @Autowired
    private BgmMapper bgmMapper;

    @Override
    public List<Bgm> queryBgmList() {
        return bgmMapper.selectAll();
    }

    @Override
    public Bgm queryBgmById(String bgmId) {
        return bgmMapper.selectByPrimaryKey(bgmId);
    }
}
