package com.huayushu.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.huayushu.mapper.*;
import com.huayushu.pojo.*;
import com.huayushu.pojo.vo.CommentsVO;
import com.huayushu.pojo.vo.VideosVO;
import com.huayushu.service.BgmService;
import com.huayushu.service.VideoService;
import com.huayushu.utils.JSONResult;
import com.huayushu.utils.PagedResult;
import com.huayushu.utils.TimeAgoUtils;
import org.apache.commons.lang3.StringUtils;
import org.n3r.idworker.Sid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

/**
 * @author: huayushu luming
 * @date: 2019-11-29 01:29
 * @desc:
 **/
@Service
public class VideoServiceImpl implements VideoService {

    @Autowired
    private VideosMapper videosMapper;
    @Autowired
    private Sid sid;
    @Autowired
    private VideosMapperCustom videosMapperCustom;
    @Autowired
    private SearchRecordsMapper searchRecordsMapper;
    @Autowired
    private UsersLikeVideosMapper usersLikeVideosMapper;
    @Autowired
    private UsersMapper usersMapper;
    @Autowired
    private CommentsMapper commentMapper;
    @Autowired
    private CommentsMapperCustom commentsMapperCustom;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public String  saveVideo(Videos videos) {
        String id=sid.nextShort();
        videos.setId(id);
        videosMapper.insertSelective(videos);
        return id;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void updateVideo(String videoId, String coverPath) {
        Videos videos=new Videos();
        videos.setId(videoId);
        videos.setCoverPath(coverPath);
       videosMapper.updateByPrimaryKeySelective(videos);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public PagedResult getAllVideos(Videos videos,Integer isSaveRecord,Integer page, Integer pageSize) {
        //保存热搜词
        String desc= videos.getVideoDesc();
        String userId=videos.getUserId();
        if(isSaveRecord!=null&&isSaveRecord==1){
            SearchRecords searchRecords=new SearchRecords();
            String recordId=sid.nextShort();
            searchRecords.setId(recordId);
            searchRecords.setContent(desc);
            searchRecordsMapper.insert(searchRecords);
        }

        PageHelper.startPage(page,pageSize);
        List<VideosVO>list=videosMapperCustom.queryAllVideos(desc,userId);
        PageInfo<VideosVO>pageList=new PageInfo<>(list);
        PagedResult pagedResult=new PagedResult();
        pagedResult.setPage(page);
        pagedResult.setTotal(pageList.getPages());
        pagedResult.setRows(list);
        pagedResult.setRecords(pageList.getTotal());
        return pagedResult;
    }

    @Override
    public List<String> getHotwords() {
        return  searchRecordsMapper.getHotwords();
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public PagedResult queryMyLikeVideos(String userId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        List<VideosVO> list = videosMapperCustom.queryMyLikeVideos(userId);

        PageInfo<VideosVO> pageList = new PageInfo<>(list);

        PagedResult pagedResult = new PagedResult();
        pagedResult.setTotal(pageList.getPages());
        pagedResult.setRows(list);
        pagedResult.setPage(page);
        pagedResult.setRecords(pageList.getTotal());

        return pagedResult;
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public PagedResult queryMyFollowVideos(String userId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        List<VideosVO> list = videosMapperCustom.queryMyFollowVideos(userId);

        PageInfo<VideosVO> pageList = new PageInfo<>(list);

        PagedResult pagedResult = new PagedResult();
        pagedResult.setTotal(pageList.getPages());
        pagedResult.setRows(list);
        pagedResult.setPage(page);
        pagedResult.setRecords(pageList.getTotal());

        return pagedResult;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void userLikeVideo(String userId, String videoId, String videoCreaterId) {
        /**
         *1. 保存用户和视频喜欢关联关系表
         */
        String likeId=sid.nextShort();
        UsersLikeVideos ulv=new UsersLikeVideos();
        ulv.setId(likeId);
        ulv.setUserId(userId);
        ulv.setVideoId(videoId);
        usersLikeVideosMapper.insert(ulv);
        /**
         * 2.视频喜欢数量累加
         */
        videosMapperCustom.addVideoLikeCount(videoId);
        /**
         * 3.用户受喜欢数量增加
         */
        usersMapper.addReceiveLikeCount(userId);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void userUnLikeVideo(String userId, String videoId, String videoCreaterId) {
        /**
         *1. 删除用户和视频的喜欢点赞关联关系表
         */
        Example example=new Example(UsersLikeVideos.class);
        Example.Criteria criteria=example.createCriteria();
        criteria.andEqualTo("userId",userId);
        criteria.andEqualTo("videoId",videoId);

        usersLikeVideosMapper.deleteByExample(example);

        /**
         * 2.视频不喜欢数量累加
         */
        videosMapperCustom.reduceVideoLikeCount(videoId);
        /**
         * 3.用户受不喜欢数量增加
         */
        usersMapper.reduceReceiveLikeCount(userId);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void saveComment(Comments comments) {
        String id=sid.nextShort();
        comments.setId(id);
        comments.setCreateTime(new Date());
        commentMapper.insert(comments);
    }
    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public PagedResult getAllComments(String videoId, Integer page, Integer pageSize) {
        PageHelper.startPage(page,pageSize);
        List<CommentsVO>list=commentsMapperCustom.queryComments(videoId);
        for(CommentsVO c:list){
            String timeAgo= TimeAgoUtils.format(c.getCreateTime());
            c.setTimeAgoStr(timeAgo);
        }
        PageInfo<CommentsVO>pageInfoList=new PageInfo<>(list);
        PagedResult grid=new PagedResult();
        grid.setTotal(pageInfoList.getPages());
        grid.setRows(list);
        grid.setPage(page);
        grid.setRecords(pageInfoList.getTotal());
        return grid;
    }


}
