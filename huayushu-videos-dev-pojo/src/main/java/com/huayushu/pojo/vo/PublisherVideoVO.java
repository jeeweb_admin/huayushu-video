package com.huayushu.pojo.vo;

import io.swagger.annotations.ApiModel;


@ApiModel(value = "发布用户对象",description ="这是用户对象")
public class PublisherVideoVO {
    public UsersVO getPublisher() {
        return publisher;
    }

    public void setPublisher(UsersVO publisher) {
        this.publisher = publisher;
    }

    public boolean isUserLikeVideo() {
        return userLikeVideo;
    }

    public void setUserLikeVideo(boolean userLikeVideo) {
        this.userLikeVideo = userLikeVideo;
    }

    public UsersVO publisher;
    public boolean userLikeVideo;

}