package com.huayushu.pojo.vo;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

public class VideosVO {
    /**
     * 主键id
     */

    private String id;

    /**
     * 用户id
     */

    private String userId;

    /**
     * 音频id
     */

    private String audioId;

    /**
     * 视频描述
     */

    private String videoDesc;

    /**
     * 视频路径
     */
    private String videoPath;

    /**
     * 视频播放秒数
     */
    private Float videoSeconds;

    /**
     * 视频宽
     */

    private Integer videoWidth;

    /**
     * 视频高
     */

    private Integer videoHeight;

    /**
     * 视频封面
     */
    private String coverPath;

    /**
     * 喜欢数量
     */
    private Long likeCounts;

    /**
     * 视频状态（1发布成功，2，禁止播放）
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 头像路径
     */
    private String faceImage;
    /**
     * 昵称
     */
    private String nickname;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 获取音频id
     *
     * @return audio_id - 音频id
     */
    public String getAudioId() {
        return audioId;
    }

    /**
     * 设置音频id
     *
     * @param audioId 音频id
     */
    public void setAudioId(String audioId) {
        this.audioId = audioId;
    }

    /**
     * 获取视频描述
     *
     * @return video_desc - 视频描述
     */
    public String getVideoDesc() {
        return videoDesc;
    }

    /**
     * 设置视频描述
     *
     * @param videoDesc 视频描述
     */
    public void setVideoDesc(String videoDesc) {
        this.videoDesc = videoDesc;
    }

    /**
     * 获取视频路径
     *
     * @return video_path - 视频路径
     */
    public String getVideoPath() {
        return videoPath;
    }

    /**
     * 设置视频路径
     *
     * @param videoPath 视频路径
     */
    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    /**
     * 获取视频播放秒数
     *
     * @return video_seconds - 视频播放秒数
     */
    public Float getVideoSeconds() {
        return videoSeconds;
    }

    /**
     * 设置视频播放秒数
     *
     * @param videoSeconds 视频播放秒数
     */
    public void setVideoSeconds(Float videoSeconds) {
        this.videoSeconds = videoSeconds;
    }

    /**
     * 获取视频宽
     *
     * @return video_width - 视频宽
     */
    public Integer getVideoWidth() {
        return videoWidth;
    }

    /**
     * 设置视频宽
     *
     * @param videoWidth 视频宽
     */
    public void setVideoWidth(Integer videoWidth) {
        this.videoWidth = videoWidth;
    }

    /**
     * 获取视频高
     *
     * @return video_height - 视频高
     */
    public Integer getVideoHeight() {
        return videoHeight;
    }

    /**
     * 设置视频高
     *
     * @param videoHeight 视频高
     */
    public void setVideoHeight(Integer videoHeight) {
        this.videoHeight = videoHeight;
    }

    /**
     * 获取视频封面
     *
     * @return cover_path - 视频封面
     */
    public String getCoverPath() {
        return coverPath;
    }

    /**
     * 设置视频封面
     *
     * @param coverPath 视频封面
     */
    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }

    /**
     * 获取喜欢数量
     *
     * @return like_counts - 喜欢数量
     */
    public Long getLikeCounts() {
        return likeCounts;
    }

    /**
     * 设置喜欢数量
     *
     * @param likeCounts 喜欢数量
     */
    public void setLikeCounts(Long likeCounts) {
        this.likeCounts = likeCounts;
    }

    /**
     * 获取视频状态（1发布成功，2，禁止播放）
     *
     * @return status - 视频状态（1发布成功，2，禁止播放）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置视频状态（1发布成功，2，禁止播放）
     *
     * @param status 视频状态（1发布成功，2，禁止播放）
     */
    public void setStatus(Integer  status) {
        this.status = status;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getFaceImage() {
        return faceImage;
    }

    public void setFaceImage(String faceImage) {
        this.faceImage = faceImage;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

}