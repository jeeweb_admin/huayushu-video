package com.huayushu.pojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "users_report")
public class UsersReport {
    /**
     * 主键id
     */
    @Id
    private String id;

    /**
     * 被举报人id
     */
    @Column(name = "deal_user_id")
    private String dealUserId;

    /**
     * 被举报视频id
     */
    @Column(name = "deal_video_id")
    private String dealVideoId;

    /**
     * 举报标题
     */
    private String title;

    /**
     * 举报内容
     */
    private String content;

    /**
     * 举报人id
     */
    private String userid;

    /**
     * 举报时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取被举报人id
     *
     * @return deal_user_id - 被举报人id
     */
    public String getDealUserId() {
        return dealUserId;
    }

    /**
     * 设置被举报人id
     *
     * @param dealUserId 被举报人id
     */
    public void setDealUserId(String dealUserId) {
        this.dealUserId = dealUserId;
    }

    /**
     * 获取被举报视频id
     *
     * @return deal_video_id - 被举报视频id
     */
    public String getDealVideoId() {
        return dealVideoId;
    }

    /**
     * 设置被举报视频id
     *
     * @param dealVideoId 被举报视频id
     */
    public void setDealVideoId(String dealVideoId) {
        this.dealVideoId = dealVideoId;
    }

    /**
     * 获取举报标题
     *
     * @return title - 举报标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置举报标题
     *
     * @param title 举报标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取举报内容
     *
     * @return content - 举报内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置举报内容
     *
     * @param content 举报内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取举报人id
     *
     * @return userid - 举报人id
     */
    public String getUserid() {
        return userid;
    }

    /**
     * 设置举报人id
     *
     * @param userid 举报人id
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * 获取举报时间
     *
     * @return create_date - 举报时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置举报时间
     *
     * @param createDate 举报时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}