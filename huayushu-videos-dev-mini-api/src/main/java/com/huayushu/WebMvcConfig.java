package com.huayushu;

import com.huayushu.controller.interceptor.MiniInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author: huayushu luming
 * @date: 2019-11-29 20:44
 * @desc:
 **/
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/META-INF/resources/")
                .addResourceLocations("file:E:/huayushu_video_dev/");
    }

    @Bean(initMethod="init")
    public ZKCuratorClient zkCuratorClient() {
        return new ZKCuratorClient();
    }

    @Bean
    public MiniInterceptor miniInterceptor() {
        return new MiniInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(miniInterceptor()).addPathPatterns("/user/**").
                addPathPatterns("/video/upload", "/video/uploadCover","/video/userLike", "/video/userUnLike","/video/saveComment")
        .addPathPatterns("/bgm/**").excludePathPatterns("/user/queryPublisher");
        super.addInterceptors(registry);
    }
}
