package com.huayushu.controller;

import com.huayushu.service.BgmService;
import com.huayushu.utils.JSONResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: huayushu luming
 * @date: 2019-11-28 19:07
 * @desc:
 **/
@RestController
@Api(value="Bgm相关业务的接口", tags= {"Bgm相关业务的controller"})
@RequestMapping("/bgm")
public class BgmController extends BasicController {
    @Autowired
    private BgmService bgmService;

    @ApiOperation(value = "查询Bgm列表", notes = "查询Bgm列表信息接口")
    @PostMapping("/list")
    public JSONResult list() throws Exception {
        return JSONResult.ok(bgmService.queryBgmList());
    }
}
