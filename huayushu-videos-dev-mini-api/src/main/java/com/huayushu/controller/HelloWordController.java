package com.huayushu.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: huayushu luming
 * @date: 2019-11-28 19:07
 * @desc:
 **/
@RestController
public class HelloWordController {
    @RequestMapping("/hello")
    public String Hello(){
    return "蔡徐坤向你问好!";
    }
}
