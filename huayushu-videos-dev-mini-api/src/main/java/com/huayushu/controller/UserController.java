package com.huayushu.controller;

import com.huayushu.pojo.Users;
import com.huayushu.pojo.UsersReport;
import com.huayushu.pojo.vo.PublisherVideoVO;
import com.huayushu.pojo.vo.UsersVO;
import com.huayushu.service.UserService;
import com.huayushu.utils.JSONResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author: huayushu luming
 * @date: 2019-11-28 19:07
 * @desc:
 **/
@RestController
@Api(value="用户相关业务的接口", tags= {"用户相关业务的controller"})
@RequestMapping("/user")
public class UserController extends BasicController {
    @Autowired
    private UserService userService;

    @ApiOperation(value = "用户上传头像", notes = "用户上传头像接口")
    @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "String", paramType = "query")
    @PostMapping("/uploadFace")
    public JSONResult uplaodFace(String userId, @RequestParam("file") MultipartFile[] files) throws Exception {
        //入参判断
        if (StringUtils.isBlank(userId)) {
            return JSONResult.errorMsg("用户id不能为空...");
        }
        //文件保存的命名空间
        String fileSpace = "E:/huayushu_video_dev";
        //保存到数据库中的相对路径
        String uploadPathDB = "/" + userId + "/face";

        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        try {
            if (files != null && files.length > 0) {
                String fileName = files[0].getOriginalFilename();
                if (StringUtils.isNotBlank(fileName)) {
                    //文件上传的最终保存路径
                    String finalFacePath = fileSpace + uploadPathDB + "/" + fileName;
                    //设置数据库保存的路径
                    uploadPathDB += ("/" + fileName);

                    File outFile = new File(finalFacePath);
                    if (outFile.getParentFile() != null || !outFile.getParentFile().isDirectory()) {
                        //创建父文件夹
                        outFile.getParentFile().mkdirs();
                    }
                    fileOutputStream=new FileOutputStream(outFile);
                    inputStream=files[0].getInputStream();
                    IOUtils.copy(inputStream,fileOutputStream);
                }
            }else {
                return JSONResult.errorMsg("上传出错了！");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return JSONResult.errorMsg("上传出错了！");
        }finally {
            if (fileOutputStream != null) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        }
        Users users=new Users();
        users.setId(userId);
        users.setFaceImage(uploadPathDB);
        userService.updateUserInfo(users);

        return JSONResult.ok(uploadPathDB);
    }


    @ApiOperation(value = "查询用户信息", notes = "查询用户信息接口")
    @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "String", paramType = "query")
    @PostMapping("/query")
    public JSONResult query(String userId,String fanId) throws Exception {
        //入参判断
        if (StringUtils.isBlank(userId)) {
            return JSONResult.errorMsg("用户id不能为空...");
        }
        Users users=userService.queryUserInfo(userId);
        UsersVO usersVO=new UsersVO();
        BeanUtils.copyProperties(users,usersVO);
        usersVO.setFollow(userService.queryIfFollw(userId,fanId));
        return JSONResult.ok(usersVO);
    }


    @ApiOperation(value = "查询发布者信息", notes = "查询发布者信息接口")
    @ApiImplicitParam(name = "loginUserId", value = "当前登录用户id", required = true, dataType = "String", paramType = "query")
    @PostMapping("/queryPublisher")
    public JSONResult queryPublisher(String loginUserId,String videoId,String publisherUserId) throws Exception {
        //入参判断
        if (StringUtils.isBlank(publisherUserId)) {
            return JSONResult.errorMsg("参数异常！");
        }
        //1.查询视频发布者得信息
        Users users=userService.queryUserInfo(publisherUserId);
        UsersVO publisher=new UsersVO();
        BeanUtils.copyProperties(users,publisher);

        //2.查询当前登录者和当前视频点赞关系
        boolean userLikeVideo=userService.isUserLikeVideo(loginUserId,videoId);
        PublisherVideoVO publisherVideoVO=new PublisherVideoVO();
        publisherVideoVO.setPublisher(publisher);
        publisherVideoVO.setUserLikeVideo(userLikeVideo);

        return JSONResult.ok(publisherVideoVO);
    }


    @ApiOperation(value = "关注用户功能", notes = "关注用户功能")
    @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "String", paramType = "query")
    @PostMapping("/beyourfans")
    public JSONResult beyourfans(String userId,String fanId) throws Exception {
        //入参判断
        if (StringUtils.isBlank(userId)||StringUtils.isBlank(fanId)) {
            return JSONResult.errorMsg("缺少参数！");
        }
        userService.saveUserFanRelation(userId,fanId);
        return JSONResult.ok("关注成功！");
    }

    @ApiOperation(value = "取消关注用户功能", notes = "取消关注用户功能")
    @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "String", paramType = "query")
    @PostMapping("/dontbeyourfans")
    public JSONResult dontbeyourfans(String userId,String fanId) throws Exception {
        //入参判断
        if (StringUtils.isBlank(userId)||StringUtils.isBlank(fanId)) {
            return JSONResult.errorMsg("缺少参数！");
        }
        userService.deleteUserFanRelation(userId,fanId);
        return JSONResult.ok("取消关注成功！");
    }
    @ApiOperation(value = "举报用户视频功能", notes = "举报用户视频功能")
    @PostMapping("/reportUser")
    public JSONResult reportUser(@RequestBody UsersReport usersReport)throws Exception{
        //保存举报信息
        userService.reportUser(usersReport);
        return JSONResult.errorMsg("举报成功，维护平台环境，人人有责！");
    }

}
