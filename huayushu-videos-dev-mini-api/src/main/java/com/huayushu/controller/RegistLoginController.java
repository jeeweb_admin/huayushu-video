package com.huayushu.controller;

import com.huayushu.pojo.Users;
import com.huayushu.pojo.vo.UsersVO;
import com.huayushu.service.UserService;
import com.huayushu.utils.JSONResult;
import com.huayushu.utils.MD5Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author: huayushu luming
 * @date: 2019-11-28 19:07
 * @desc:用户注册Controller
 **/
@RestController
@Api(value = "用户注册登录接口", tags = {"注册和登录的controller"})
public class RegistLoginController extends BasicController {
    @Autowired
    private UserService userService;

    @ApiOperation(value = "用户注册接口", notes = "用户注册的接口")
    @PostMapping("/regist")
    public JSONResult regist(@RequestBody Users users) throws Exception {
        //1.判断用户名和密码不为空
        if (StringUtils.isBlank(users.getUsername()) || StringUtils.isBlank(users.getPassword())) {
            return JSONResult.errorMsg("用户名和密码不能为空");
        }

        //2.判断用户名是否存在
        boolean usernameIsExist = userService.queryUsernameIsExist(users.getUsername());

        //3.保存用户注册信息
        if (!usernameIsExist) {
            users.setNickname(users.getUsername());
            users.setPassword(MD5Utils.getMD5Str(users.getPassword()));
            userService.saveUser(users);
        } else {
            return JSONResult.errorMsg("用户名已存在，请换一个再试！");
        }
        users.setPassword(null);

        UsersVO usersVO = setUserRedisSessionToken(users);
        return JSONResult.ok(usersVO);
    }

    public UsersVO setUserRedisSessionToken(Users users) {
        String uniqueToken = UUID.randomUUID().toString();
        redisOperator.set(USER_REDIS_SESSION + ":" + users.getId(), uniqueToken, 1000 * 60 * 30);

        UsersVO usersVO = new UsersVO();
        BeanUtils.copyProperties(users, usersVO);
        usersVO.setUserToken(uniqueToken);
        return usersVO;
    }

    @ApiOperation(value = "用户登录", notes = "用户登录的接口")
    @PostMapping("/login")
    public JSONResult login(@RequestBody Users users) throws Exception {
        String username = users.getUsername();
        String password = users.getPassword();

        //Thread.sleep(3000);
        //1.判断用户名和密码必须不为空
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            return JSONResult.ok("用户名或密码不能为空...");
        }
        //2.判断用户是否存在
        Users userResult = userService.queryUserForLogin(username, MD5Utils.getMD5Str(password));
        //3.返回
        if (userResult != null) {
            userResult.setPassword(null);
            UsersVO usersVO = setUserRedisSessionToken(userResult);
            return JSONResult.ok(usersVO);
        } else {
            return JSONResult.errorMsg("用户名或密码不正确,请重试！");
        }


    }


    @ApiOperation(value = "用户注销", notes = "用户注销的接口")
    @ApiImplicitParam(name="userId",value = "用户id",required = true,dataType = "String",paramType = "query")
    @PostMapping("/logout")
    public JSONResult logout(String userId) throws Exception {
        redisOperator.del(USER_REDIS_SESSION + ":" + userId);
        return JSONResult.ok();
    }
}
