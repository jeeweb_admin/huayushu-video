package com.huayushu.controller;

import com.huayushu.utils.RedisOperator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: huayushu luming
 * @date: 2019-11-28 19:07
 * @desc:基础Controller
 **/
@RestController
public class BasicController {
    @Autowired
    public RedisOperator redisOperator;

    public static final String USER_REDIS_SESSION="user-redis-session";
    //文件保存的命名空间
    public static final String FILE_SPACE="E:/huayushu_video_dev";
    //ffmpeg所在目录
    public static final String FFMPEG_EXE="D:\\soft\\ffmpeg\\bin\\ffmpeg.exe";
    //每页的分页记录数
    public  static  final  Integer PAGE_SIZE=5;
}
